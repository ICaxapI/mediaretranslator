package ru.exsoft.mediaretranslator.net;

import android.util.Log;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SocketClient extends Listener {
    private static Client client = new Client();
    private static HashMap<String, PacketMessage> requestsQueue = new HashMap<>();
    private static String ip;
    private static int tcpPort;
    private static int udpPort;
    private static boolean started = false;
    private static boolean init = false;
    private static long lastCommand = 0;

    private void start() throws Exception {
        Log.d("100", "Start connection");
        started = true;
        client.getKryo().register(PacketMessage.class);
        client.getKryo().register(Command.class);
        client.start();
        client.connect(60000, ip, tcpPort, udpPort);
        client.addListener(new SocketClient());
        lastCommand = System.currentTimeMillis();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (started){
                    if (System.currentTimeMillis() - lastCommand > 30000){
                        stop();
                    } else {
                        try {
                            TimeUnit.SECONDS.sleep(30);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    public void start(String ip, int tcpPort, int udpPort) throws Exception {
        if (!init) {
            init = true;
            SocketClient.ip = ip;
            SocketClient.tcpPort = tcpPort;
            SocketClient.udpPort = udpPort;
            start();
        } else if (started){
            if (!(SocketClient.ip.equals(ip) && SocketClient.tcpPort == tcpPort && SocketClient.udpPort == udpPort)){
                stop();
                SocketClient.ip = ip;
                SocketClient.tcpPort = tcpPort;
                SocketClient.udpPort = udpPort;
                start();
            }
        } else {
            if (!(SocketClient.ip.equals(ip) && SocketClient.tcpPort == tcpPort && SocketClient.udpPort == udpPort)){
                SocketClient.ip = ip;
                SocketClient.tcpPort = tcpPort;
                SocketClient.udpPort = udpPort;
                start();
            } else {
                start();
            }
        }
    }

    public boolean sendCommand(String passwordMD5, Command command){
        lastCommand = System.currentTimeMillis();
        Log.d("100", "send command " + command.name());
        PacketMessage packetMessage = new PacketMessage();
        packetMessage.command = command;
        packetMessage.password = passwordMD5;
        packetMessage.nano = System.nanoTime();
        requestsQueue.put(packetMessage.nano.toString(), packetMessage);
        client.sendTCP(packetMessage);
        synchronized (requestsQueue.get(packetMessage.nano.toString())){
            PacketMessage qeued = requestsQueue.get(packetMessage.nano.toString());
            requestsQueue.remove(qeued.nano);
            synchronized (qeued) {
                try {
                    qeued.wait();
                    return qeued.message.equals("ok");
                } catch (InterruptedException e) {
                    return false;
                }
            }
        }
    }

    public InetAddress findLocalServer(int udpPort){
        InetAddress address = client.discoverHost(udpPort, 5000);
        return address;
    }

    public void received(Connection c, Object p) {
        if (p instanceof PacketMessage) {
            PacketMessage packet = (PacketMessage) p;
            Log.d("100", "receive response! " + packet.command + " " + packet.message);
            PacketMessage qeued = requestsQueue.get(packet.nano.toString());
            if (qeued != null){
                synchronized (qeued) {
                    qeued.message = packet.message;
                    qeued.notify();
                }
            }
        }
    }

    private void stop(){
        Log.d("100", "Stop connection");
        started = false;
        client.close();
        client.stop();
    }
}

package ru.exsoft.mediaretranslator

import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import ru.exsoft.mediaretranslator.net.Command
import ru.exsoft.mediaretranslator.net.SocketClient
import android.app.*
import android.graphics.Color
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.exsoft.mediaretranslator.R
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.app.PendingIntent
import android.content.*
import androidx.media.session.MediaButtonReceiver
import android.media.*
import android.media.session.PlaybackState.PLAYBACK_POSITION_UNKNOWN
import android.os.*
import android.support.v4.media.session.PlaybackStateCompat.*
import android.widget.RemoteViews


class RetranslatorService : Service() {

    private var mSettings: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    private var ip: String? = null
    private var porttcp: Int? = null
    private var portudp: Int? = null
    private var md5Pass: String? = null

    private var currentMode = 14
    private var shutdownInit = false
    private var playing = false

    private var audioManager: AudioManager? = null
    private val serviceBinder = ServiceBinder()
    private var volumeChangeReceiver: BroadcastReceiver? = null

    private var remoteViews: RemoteViews? = null

    private val metadataBuilder = MediaMetadataCompat.Builder()
    private val stateBuilder = PlaybackStateCompat.Builder().setActions(
        PlaybackStateCompat.ACTION_PLAY
                or PlaybackStateCompat.ACTION_STOP
                or PlaybackStateCompat.ACTION_PAUSE
                or PlaybackStateCompat.ACTION_PLAY_PAUSE
                or PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                or PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    )
    private var mediaSession: MediaSessionCompat? = null

    private var audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener =
        AudioManager.OnAudioFocusChangeListener {
            fun onAudioFocusChange(focusChange: Int) {
                Log.d("100", "onAudioFocusChange")
                when (focusChange) {
                    AudioManager.AUDIOFOCUS_GAIN -> mediaSessionCallback.onPlay()
                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> mediaSessionCallback.onPause()
                    else -> mediaSessionCallback.onPause()
                }

            }
        }

    override fun onBind(p0: Intent?): IBinder? {
        Log.d("100", "service bind")
        return serviceBinder
    }

    override fun onUnbind(p0: Intent?): Boolean {
        Log.d("100", "service ubind")
        return false
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when(intent.action){
            "playpause" -> playpause(14)
            "next" -> nextPrev(true, 14)
            "prev" -> nextPrev(false, 14)
            "vup" -> nextPrev(true, 15)
            "vdown" -> nextPrev(false, 15)
        }
        MediaButtonReceiver.handleIntent(mediaSession, intent)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        loadPrefs()
        displayNotification()
        createMediaSession()
        registerVolumeChangeReceiver()
        Log.d("100", "onCreate")
    }

    fun getModeStr(value: Int): String? {
        val ret: String?
        when (value){
            0 -> ret = "Shutdown"
            1 -> ret = "Screen-off"
            2 -> ret = "Lock"
            3 -> ret = "Sleep"
            15 -> ret = "Volume changing"
            else -> ret = "Standart"
        }
        Log.d("999", value.toString())
        return ret
    }

    private var onProcessing = false
    private fun registerVolumeChangeReceiver(){
        volumeChangeReceiver = registerReceiver(IntentFilter("android.media.VOLUME_CHANGED_ACTION")) { intent ->
            if (!onProcessing) {
                onProcessing = true
                when (intent?.action) {
                    "android.media.VOLUME_CHANGED_ACTION" -> {
                        val newVolume =
                            intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0)
                        val oldVolume =
                            intent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", 0)

                        if (newVolume != oldVolume) {
                            Log.d(
                                "100",
                                "In onReceive  newVolume: $newVolume oldVolume: $oldVolume"
                            )
                            changeMode(newVolume)
                        }
                    }
                }
                onProcessing = false
            }
        }
    }

    private fun changeMode(add: Boolean){
        if (add){
            changeMode(currentMode + 1)
        } else {
            changeMode(currentMode - 1)
        }
    }

    private fun changeMode(newVolume: Int){
        currentMode = newVolume
        val metadata = metadataBuilder
            .putString(MediaMetadataCompat.METADATA_KEY_TITLE, "Mode: " + getModeStr(currentMode))
            .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "ExSoft")
            .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "ExSoft")
            .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, 10)
            .build()
        mediaSession?.setMetadata(metadata)
        displayNotification()
    }

    private fun Context.registerReceiver(intentFilter: IntentFilter, onReceive: (intent: Intent?) -> Unit): BroadcastReceiver {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                onReceive(intent)
            }
        }
        this.registerReceiver(receiver, intentFilter)
        return receiver
    }

    private fun createMediaSession() {
        mediaSession = MediaSessionCompat(applicationContext, packageName)

        val componentName =
            ComponentName(applicationContext, RetranslatorService::class.java)
        val mediaButtonIntent = Intent(Intent.ACTION_MEDIA_BUTTON)
        mediaButtonIntent.component = componentName
        val mediaButtonReceiverPendingIntent =
            PendingIntent.getBroadcast(applicationContext, 0, mediaButtonIntent, 0)
        mediaSession!!.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
        mediaSession!!.setCallback(mediaSessionCallback)

        val appContext = applicationContext
        val serviceIntent = Intent(appContext, RetranslatorService::class.java)
        mediaSession!!.setSessionActivity(
            PendingIntent.getActivity(
                appContext,
                0,
                serviceIntent,
                0
            )
        )
        mediaSession!!.setMediaButtonReceiver(
            mediaButtonReceiverPendingIntent
        )
        val metadata = metadataBuilder
            .putString(MediaMetadataCompat.METADATA_KEY_TITLE, "Mode " + getModeStr(currentMode))
            .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "ExSoft")
            .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "ExSoft")
            .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, 10)
            .build()
        mediaSession?.setMetadata(metadata)
        val state = Builder().setActions(
            ACTION_PLAY or ACTION_PLAY_PAUSE or
                    ACTION_PLAY_FROM_MEDIA_ID or ACTION_PAUSE or
                    ACTION_SKIP_TO_NEXT or ACTION_SKIP_TO_PREVIOUS
        ).setState(STATE_PLAYING, 1, 1.0f, SystemClock.elapsedRealtime())
            .build()
        mediaSession?.setPlaybackState(state)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
            val audioFocusRequest = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttributes)
                .setOnAudioFocusChangeListener(audioFocusChangeListener)
                .build()
            audioManager!!.requestAudioFocus(audioFocusRequest)
        } else {
            audioManager!!.requestAudioFocus(
                audioFocusChangeListener,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN
            )
        }
        mediaSession!!.isActive = true
        val dummyAudioTrack = AudioTrack(
            AudioManager.STREAM_MUSIC,
            48000,
            AudioFormat.CHANNEL_OUT_STEREO,
            AudioFormat.ENCODING_PCM_16BIT,
            AudioTrack.getMinBufferSize(
                48000,
                AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT
            ),
            AudioTrack.MODE_STREAM
        )
        dummyAudioTrack.play()
    }

    private fun displayNotification() {
        val notification: Notification
        try {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("com.audioanywhere.from_notification", true)

            val intent2 = Intent(this, RetranslatorService::class.java)
            intent2.action = "playpause"
            val service = PendingIntent.getService(this, 1, intent2, 0)

            val intent3 = Intent(this, RetranslatorService::class.java)
            intent3.action = "prev"
            val service2 = PendingIntent.getService(this, 1, intent3, 0)

            val intent4 = Intent(this, RetranslatorService::class.java)
            intent4.action = "next"
            val service3 = PendingIntent.getService(this, 1, intent4, 0)

            val intent5 = Intent(this, RetranslatorService::class.java)
            intent5.action = "vup"
            val service4 = PendingIntent.getService(this, 1, intent5, 0)

            val intent6 = Intent(this, RetranslatorService::class.java)
            intent6.action = "vdown"
            val service5 = PendingIntent.getService(this, 1, intent6, 0)

            remoteViews = RemoteViews(packageName, R.layout.audio_notifications_layout)
            remoteViews?.setTextViewText(R.id.notif_audio_artist, "Mode: " + getModeStr(currentMode))
            remoteViews?.setTextViewText(R.id.notif_audio_title, "Desktop manager")
            remoteViews?.setOnClickPendingIntent(R.id.notif_audio_previous_button, service2)
            remoteViews?.setOnClickPendingIntent(R.id.notif_audio_play_button, service)
            remoteViews?.setOnClickPendingIntent(R.id.notif_audio_next_button, service3)
            remoteViews?.setOnClickPendingIntent(R.id.notif_audio_vol_up, service4)
            remoteViews?.setOnClickPendingIntent(R.id.notif_audio_vol_down, service5)

            val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createNotificationChannel(
                    "my_service",
                    "My Background Service"
                )
                else ""
            val builder = NotificationCompat.Builder(this, channelId)
            builder.setSmallIcon(R.drawable.player_next_button_grey)
                .setContent(remoteViews)
            builder.setContentIntent(
                PendingIntent.getActivity(
                    this,
                    1,
                    intent,
                    0
                )
            )
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            notification = builder.build()
            notification.flags = notification.flags or 2

            startForeground(10, notification)
            Log.i("100", "start foreground state")
            return
        } catch (th: Throwable) {
            th.printStackTrace()
        }
    }

    private fun stopForegroundWithNotification() {
        stopForeground(false)
    }

    private fun loadPrefs() {
        val mSettings = getSharedPreferences(LoginActivity.APP_PREFERENCES, Context.MODE_PRIVATE)
        editor = mSettings.edit()
        ip = mSettings!!.getString(LoginActivity.APP_PREFERENCES_IP, "127.0.0.1")
        porttcp = mSettings.getString(
            LoginActivity.APP_PREFERENCES_PORT_TCP,
            resources.getString(R.string.defaultport)
        )
            ?.let {
                Log.d("100", it)
                Integer.parseInt(it)
            }
        portudp = mSettings.getString(
            LoginActivity.APP_PREFERENCES_PORT_UDP,
            resources.getString(R.string.defaultport)
        )
            ?.let {
                Log.d("100", it)
                Integer.parseInt(it)
            }
        md5Pass = mSettings.getString(LoginActivity.APP_PREFERENCES_PASSWORD_MD5, "")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onDestroy() {
        Log.d("100", "onDestroy")
        unregisterReceiver(volumeChangeReceiver)
        stopForegroundWithNotification()
        super.onDestroy()
    }

    fun playpause(){
        playpause(currentMode)
    }

    fun playpause(mode: Int){
        if (!onProcessing) {
            onProcessing = true
            Log.d("100", "onPlay")
            if (playing) {
                mediaSession?.setPlaybackState(
                    stateBuilder.setState(
                        STATE_PAUSED,
                        PLAYBACK_POSITION_UNKNOWN, 1.0f
                    ).build()
                )
            } else {
                mediaSession?.setPlaybackState(
                    stateBuilder.setState(
                        STATE_PLAYING,
                        PLAYBACK_POSITION_UNKNOWN, 1.0f
                    ).build()
                )
            }
            playing = !playing
            Thread(
                Runnable {
                    run {
                        if (mode > 3) {
                            val client = SocketClient()
                            client.start(
                                ip,
                                Integer.valueOf(porttcp!!),
                                Integer.valueOf(portudp!!)
                            )
                            val result = client.sendCommand(md5Pass, Command.PLAYPAUSE)
                        } else if (mode == 3) {
                            val client = SocketClient()
                            client.start(
                                ip,
                                Integer.valueOf(porttcp!!),
                                Integer.valueOf(portudp!!)
                            )
                            val result = client.sendCommand(md5Pass, Command.SLEEP)
                        } else if (mode == 2) {
                            val client = SocketClient()
                            client.start(
                                ip,
                                Integer.valueOf(porttcp!!),
                                Integer.valueOf(portudp!!)
                            )
                            val result = client.sendCommand(md5Pass, Command.LOCK)
                        } else if (mode == 1) {
                            val client = SocketClient()
                            client.start(
                                ip,
                                Integer.valueOf(porttcp!!),
                                Integer.valueOf(portudp!!)
                            )
                            val result = client.sendCommand(md5Pass, Command.SCREEN_OFF)
                        } else if (mode == 0) {
                            val client = SocketClient()
                            client.start(
                                ip,
                                Integer.valueOf(porttcp!!),
                                Integer.valueOf(portudp!!)
                            )
                            if (!shutdownInit) {
                                val result = client.sendCommand(md5Pass, Command.SHUTDOWN)
                            } else {
                                val result =
                                    client.sendCommand(md5Pass, Command.SHUTDOWN_CANCEL)
                            }
                            shutdownInit = !shutdownInit
                        }
                    }
                }
            ).start()
            onProcessing = false
        }
    }

    fun nextPrev(next: Boolean){
        nextPrev(next, currentMode)
    }

    fun nextPrev(next: Boolean, mode: Int){
        Log.d("100", "onSkipToNext")
        Thread(
            Runnable {
                run {
                    if (mode != 15) {
                        val client = SocketClient()
                        client.start(
                            ip,
                            Integer.valueOf(porttcp!!),
                            Integer.valueOf(portudp!!)
                        )
                        if (next) {
                            val result = client.sendCommand(md5Pass, Command.NEXT)
                        } else {
                            val result = client.sendCommand(md5Pass, Command.PREVIOUS)
                        }
                    } else {
                        val client = SocketClient()
                        client.start(
                            ip,
                            Integer.valueOf(porttcp!!),
                            Integer.valueOf(portudp!!)
                        )
                        if (next) {
                            val result = client.sendCommand(md5Pass, Command.VOLUME_UP)
                        } else {
                            val result = client.sendCommand(md5Pass, Command.VOLUME_DOWN)
                        }
                    }
                }
            }
        ).start()
    }

    private var mediaSessionCallback: MediaSessionCompat.Callback =
        object : MediaSessionCompat.Callback() {

            override fun onMediaButtonEvent(mediaButtonEvent: Intent?): Boolean {
                Log.d("100", "onMediaButtonEvent")
                return super.onMediaButtonEvent(mediaButtonEvent)
            }

            override fun onPlay() {
                playpause()
            }

            override fun onPause() {
                playpause()
            }

            override fun onSkipToNext() {
                nextPrev(true)
            }

            override fun onSkipToPrevious() {
                nextPrev(false)
            }
        }

    inner class ServiceBinder : Binder() {
        internal val service: RetranslatorService
            get() = this@RetranslatorService

        fun getMediaSessionToken(): MediaSessionCompat.Token {
            return mediaSession!!.getSessionToken()
        }
    }
}
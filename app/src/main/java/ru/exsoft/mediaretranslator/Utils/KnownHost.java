package ru.exsoft.mediaretranslator.Utils;

public class KnownHost {
    public String ip;
    public String portTCP;
    public String portUDP;
    public String passwordMD5;
    public int passwordCount;

    public KnownHost(String ip, String portTCP, String portUDP, String passwordMD5, int passwordCount){
        this.ip = ip;
        this.portTCP = portTCP;
        this.portUDP = portUDP;
        this.passwordMD5 = passwordMD5;
        this.passwordCount = passwordCount;
    }
}

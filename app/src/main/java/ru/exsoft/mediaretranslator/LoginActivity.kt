package ru.exsoft.mediaretranslator

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import kotlinx.android.synthetic.main.activity_login.*
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.exsoft.mediaretranslator.R
import ru.exsoft.mediaretranslator.net.SocketClient
import ru.exsoft.mediaretranslator.Utils.AndroidBug5497Workaround
import ru.exsoft.mediaretranslator.Utils.CryptUtils
import ru.exsoft.mediaretranslator.Utils.KnownHost
import java.lang.Exception
import java.lang.StringBuilder
import kotlin.collections.HashSet
import com.google.gson.Gson
import ru.exsoft.mediaretranslator.Utils.Utils
import ru.exsoft.mediaretranslator.net.Command
import java.util.logging.Logger

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        AndroidBug5497Workaround.assistActivity(this)

        val mSettings: SharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = mSettings.edit()

        addFilters()
        addListeners()

        if (!(mSettings.contains(APP_PREFERENCES_AUTH) && mSettings.getBoolean(APP_PREFERENCES_AUTH, false))) {
            if (mSettings.contains(APP_PREFERENCES_IP)) ip.setText(mSettings.getString(APP_PREFERENCES_IP, ""))
            if (mSettings.contains(APP_PREFERENCES_PORT_TCP)) porttcp.setText(mSettings.getString(APP_PREFERENCES_PORT_TCP, ""))
            if (mSettings.contains(APP_PREFERENCES_PORT_UDP)) portudp.setText(mSettings.getString(APP_PREFERENCES_PORT_UDP, ""))
            if (mSettings.contains(APP_PREFERENCES_PASSWORD_ASTERISK)) {
                val stringBuilder = StringBuilder("")
                for (i in 0 until mSettings.getInt(APP_PREFERENCES_PASSWORD_ASTERISK, 1)) {
                    stringBuilder.append("•")
                }
                if (mSettings.getInt(APP_PREFERENCES_PASSWORD_ASTERISK, 0) > 0){
                    password.hint = stringBuilder.toString()
                    password.setHintTextColor(Color.BLACK)
                }
                else {
                    password.hint = resources.getString(R.string.password)
                    password.setHintTextColor(Color.GRAY)
                }
            }
        } else {
            val newIntent = Intent(this, MainActivity::class.java)
            newIntent.putExtra(
                APP_PREFERENCES_PASSWORD_MD5,
                mSettings.getString(APP_PREFERENCES_PASSWORD_MD5, "")
            )
            newIntent.putExtra(
                APP_PREFERENCES_IP,
                mSettings.getString(APP_PREFERENCES_IP, "")
            )
            newIntent.putExtra(
                APP_PREFERENCES_PORT_TCP,
                mSettings.getString(
                    APP_PREFERENCES_PORT_TCP,
                    resources.getString(R.string.defaultport)
                )
            )
            newIntent.putExtra(
                APP_PREFERENCES_PORT_UDP,
                mSettings.getString(
                    APP_PREFERENCES_PORT_UDP,
                    resources.getString(R.string.defaultport)
                )
            )
            startActivity(newIntent)
            finish()
        }

    }

    private fun addKnownHost(knownHost: KnownHost){
        ksTitle.visibility = View.VISIBLE
        val mSettings: SharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = mSettings.edit()
        val cardCopylayout : LinearLayout = LayoutInflater.from(this).inflate(R.layout.known_host_fragment, knownServers, true) as LinearLayout
        val card : LinearLayout = cardCopylayout.getChildAt(cardCopylayout.childCount - 1) as LinearLayout
        val buttonsLayout : LinearLayout = card.getChildAt(1) as LinearLayout
        val serverTitle : TextView = card.getChildAt(0) as TextView
        val connectButton : Button = buttonsLayout.getChildAt(0) as Button
        val deleteButton : Button = buttonsLayout.getChildAt(1) as Button

        serverTitle.text = String.format(resources.getString(R.string.knownServerPattern), knownHost.ip, knownHost.portTCP, knownHost.portUDP)

        connectButton.setOnClickListener {
            ip.setText(knownHost.ip)
            editor.putString(APP_PREFERENCES_IP, knownHost.ip)

            porttcp.setText(knownHost.portTCP)
            editor.putString(APP_PREFERENCES_PORT_TCP, knownHost.portTCP)

            portudp.setText(knownHost.portUDP)
            editor.putString(APP_PREFERENCES_PORT_UDP, knownHost.portUDP)

            password.setText("")
            val stringBuilder1 = StringBuilder("")
            for (i in 0 until knownHost.passwordCount) {
                stringBuilder1.append("•")
            }
            if (knownHost.passwordCount > 0){
                password.hint = stringBuilder1.toString()
                password.setHintTextColor(Color.BLACK)
            } else {
                password.hint = resources.getString(R.string.password)
                password.setHintTextColor(Color.GRAY)
            }
            editor.putString(APP_PREFERENCES_PASSWORD_MD5, knownHost.passwordMD5)
            editor.putInt(APP_PREFERENCES_PASSWORD_ASTERISK, knownHost.passwordCount)
            editor.commit()
        }

        deleteButton.setOnClickListener {
            Thread(
                Runnable{
                    run {
                        val knownHostsJSON: MutableSet<String>? = mSettings.getStringSet(APP_PREFERENCES_KNOWN_HOSTS, mutableSetOf())
                        val knownHostsGSON: MutableSet<KnownHost> = HashSet()
                        val gson = Gson()
                        knownHostsJSON?.forEach{s: String -> run {
                            val kh = gson.fromJson(s, KnownHost::class.java)
                            if (!(       kh.ip == knownHost.ip &&
                                        kh.portTCP == knownHost.portTCP &&
                                        kh.portUDP == knownHost.portUDP &&
                                        kh.passwordCount == knownHost.passwordCount &&
                                        kh.passwordMD5 == knownHost.passwordMD5)){
                                knownHostsGSON.add(kh)
                                Log.d("100", "add")
                            }
                        }}
                        val knownHostsJSONNEW: MutableSet<String>? = mutableSetOf()
                        knownHostsGSON.forEach{k: KnownHost -> run {
                            knownHostsJSONNEW?.add(gson.toJson(k))}
                        }
                        editor.putStringSet(APP_PREFERENCES_KNOWN_HOSTS, knownHostsJSONNEW)
                        editor.commit()

                        runOnUiThread{
                            cardCopylayout.removeView(card)
                            if (cardCopylayout.childCount == 0) ksTitle.visibility = View.GONE
                        }
                    }
                }
            ).start()
        }
    }

    @SuppressLint("ApplySharedPref")
    private fun addListeners(){
        val mSettings: SharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = mSettings.edit()
        var previosText = ""

        mSettings.getStringSet(APP_PREFERENCES_KNOWN_HOSTS, mutableSetOf())?.forEach {s: String -> run{
                val gson = Gson()
                addKnownHost(gson.fromJson(s, KnownHost::class.java))
            }
        }

        ip.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                run {
                    editor.putString(APP_PREFERENCES_IP, ip.text.toString())
                    editor.commit()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val splits = s.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                var result = ""
                for (i in splits.indices) {
                    if (Integer.valueOf(splits[i]) > 25) {
                        result = if (i < 3) result + splits[i] + "." else result + splits[i]
                    } else {
                        result += splits[i]
                    }
                    try {
                        if (s.toString().split(result)[1].startsWith(".")) result += "."
                    } catch (ignore: Exception){

                    }
                }
                if (previosText != result){
                    previosText = result
                    ip.setText(result)
                    ip.setSelection(ip.text.toString().length)
                }
            }
        })

        porttcp.afterTextChanged {
            run {
                editor.putString(APP_PREFERENCES_PORT_TCP, porttcp.text.toString())
                editor.commit()
            }
        }

        portudp.afterTextChanged {
            run {
                editor.putString(APP_PREFERENCES_PORT_UDP, portudp.text.toString())
                editor.commit()
            }
        }

        password.afterTextChanged {
            run {
                editor.putString(
                    APP_PREFERENCES_PASSWORD_MD5,
                    CryptUtils.getMd5(password.text.toString())
                )
                editor.putInt(
                    APP_PREFERENCES_PASSWORD_ASTERISK,
                    password.text.toString().length
                )
                editor.commit()
                password.hint = resources.getString(R.string.password)
                password.setHintTextColor(Color.GRAY)
            }
        }

        find.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        runOnUiThread {
                            if (portudp.text.toString() == ""){
                                portudp.setText(R.string.defaultport)
                            }
                            if (porttcp.text.toString() == ""){
                                porttcp.setText(R.string.defaultport)
                            }
                            //constraintLayout.visibility = View.INVISIBLE
                            progressBar.visibility = View.VISIBLE
                        }
                        val client = SocketClient()
                        val address = client.findLocalServer(
                            Integer.valueOf(
                                if (portudp.text.toString() != "") portudp.text.toString()
                                else resources.getString(R.string.defaultport)
                            )
                        )
                        val addressString = if (address != null) address.canonicalHostName else ""
                        runOnUiThread {
                            if (addressString != "") {
                                Toast.makeText(
                                    this,
                                    String.format(resources.getString(R.string.findTrue), addressString),
                                    Toast.LENGTH_SHORT
                                ).show()
                                ip.setText(addressString)
                            } else {
                                Toast.makeText(
                                    this,
                                    String.format(resources.getString(R.string.findFalse), addressString),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            progressBar.visibility = View.INVISIBLE
                            //constraintLayout.visibility = View.VISIBLE
                        }
                    }
                }
            ).start()
        }

        connect.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        if (    Utils.validIP(ip.text.toString()) &&
                                porttcp.text.isNotEmpty() &&
                                portudp.text.isNotEmpty()) {
                            val client = SocketClient()
                            client.start(
                                ip.text.toString(),
                                Integer.valueOf(porttcp.text.toString()),
                                Integer.valueOf(portudp.text.toString())
                            )
                            val correct = client.sendCommand(
                                mSettings.getString(
                                    APP_PREFERENCES_PASSWORD_MD5,
                                    CryptUtils.getMd5(password.text.toString())
                                ), Command.CHECKPASS
                            )
                            runOnUiThread {
                                if (correct) {
                                    editor.putBoolean(
                                        APP_PREFERENCES_AUTH,
                                        true
                                    )

                                    val knownHostsJSON: MutableSet<String>? =
                                        mSettings.getStringSet(
                                            APP_PREFERENCES_KNOWN_HOSTS,
                                            mutableSetOf()
                                        )
                                    val knownHostsGSON: MutableSet<KnownHost> = HashSet()
                                    val gson = Gson()
                                    knownHostsJSON?.forEach { s: String ->
                                        run {
                                            knownHostsGSON.add(
                                                gson.fromJson(s, KnownHost::class.java)
                                            )
                                        }
                                    }
                                    val newHost = KnownHost(
                                        mSettings.getString(APP_PREFERENCES_IP, ""),
                                        mSettings.getString(
                                            APP_PREFERENCES_PORT_TCP,
                                            resources.getString(R.string.defaultport)
                                        ),
                                        mSettings.getString(
                                            APP_PREFERENCES_PORT_UDP,
                                            resources.getString(R.string.defaultport)
                                        ),
                                        mSettings.getString(APP_PREFERENCES_PASSWORD_MD5, ""),
                                        mSettings.getInt(APP_PREFERENCES_PASSWORD_ASTERISK, 0)
                                    )
                                    knownHostsGSON.add(newHost)
                                    val knownHostsJSONNEW: MutableSet<String>? =
                                        mSettings.getStringSet(
                                            APP_PREFERENCES_KNOWN_HOSTS,
                                            mutableSetOf()
                                        )
                                    knownHostsGSON.forEach { k: KnownHost ->
                                        run {
                                            knownHostsJSONNEW?.add(gson.toJson(k))
                                        }
                                    }
                                    editor.putStringSet(
                                        APP_PREFERENCES_KNOWN_HOSTS,
                                        knownHostsJSONNEW
                                    )
                                    editor.commit()

                                    val newIntent = Intent(this, MainActivity::class.java)
                                    newIntent.putExtra(
                                        APP_PREFERENCES_PASSWORD_MD5,
                                        mSettings.getString(APP_PREFERENCES_PASSWORD_MD5, "")
                                    )
                                    newIntent.putExtra(
                                        APP_PREFERENCES_IP,
                                        mSettings.getString(APP_PREFERENCES_IP, "")
                                    )
                                    newIntent.putExtra(
                                        APP_PREFERENCES_PORT_TCP,
                                        mSettings.getString(
                                            APP_PREFERENCES_PORT_TCP,
                                            resources.getString(R.string.defaultport)
                                        )
                                    )
                                    newIntent.putExtra(
                                        APP_PREFERENCES_PORT_UDP,
                                        mSettings.getString(
                                            APP_PREFERENCES_PORT_UDP,
                                            resources.getString(R.string.defaultport)
                                        )
                                    )
                                    startActivity(newIntent)
                                    finish()
                                } else {
                                    Toast.makeText(this, "Неверный пароль", Toast.LENGTH_SHORT)
                                        .show()
                                }
                            }
                        } else {
                            runOnUiThread {
                                Toast.makeText(this, "Неверный ввод", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                }
            ).start()
        }
    }

    private fun addFilters(){
        val filtersIp = arrayOfNulls<InputFilter>(1)
        filtersIp[0] = InputFilter { source, start, end, dest, dstart, dend ->
            if (end > start) {
                val destTxt = dest.toString()
                val resultingTxt =
                    destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend)
                if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?".toRegex())) {
                    return@InputFilter ""
                } else {
                    val splits = resultingTxt.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    for (i in splits.indices) {
                        if (Integer.valueOf(splits[i]) > 255 && source[source.length - 1] != '.') {
                            return@InputFilter ""
                        }
                    }
                }
            }
            null
        }
        ip.filters = filtersIp

        val filtersPort = arrayOfNulls<InputFilter>(1)
        filtersPort[0] = InputFilter { source, start, end, dest, dstart, dend ->
            try {
                if (end > start) {
                    val destTxt = dest.toString()
                    val resultingTxt = destTxt.substring(0, dstart) + source.subSequence(
                        start,
                        end
                    ) + destTxt.substring(dend)
                    if (Integer.valueOf(resultingTxt) > 65535) {
                        return@InputFilter ""
                    }
                }
            } catch (ex: Exception){
                return@InputFilter ""
            }
            null
        }
        porttcp.filters = filtersPort
        portudp.filters = filtersPort
    }

    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    companion object {
        const val APP_PREFERENCES = "pref"
        const val APP_PREFERENCES_IP = "ip"
        const val APP_PREFERENCES_AUTH = "auth"
        const val APP_PREFERENCES_PORT_TCP = "porttcp"
        const val APP_PREFERENCES_PORT_UDP = "portudp"
        const val APP_PREFERENCES_KNOWN_HOSTS = "hosts"
        const val APP_PREFERENCES_PASSWORD_MD5 = "password"
        const val APP_PREFERENCES_PASSWORD_ASTERISK = "asterisk"
    }
}

package ru.exsoft.mediaretranslator

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.exsoft.mediaretranslator.R
import kotlinx.android.synthetic.main.activity_main.*
import ru.exsoft.mediaretranslator.Utils.AndroidBug5497Workaround
import ru.exsoft.mediaretranslator.net.Command
import ru.exsoft.mediaretranslator.net.SocketClient
import android.content.ComponentName
import android.os.IBinder
import android.content.ServiceConnection
import android.os.CountDownTimer
import android.os.RemoteException
import android.support.v4.media.session.MediaControllerCompat
import android.util.Log
import android.view.View
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private var mSettings : SharedPreferences? = null
    private var editor : SharedPreferences.Editor? = null
    private var ip : String? = null
    private var porttcp : Int? = null
    private var portudp : Int? = null
    private var md5Pass : String? = null

    private var srv: RetranslatorService? = null
    private var playIntent : Intent? = null
    private var serviceBound = false
    private var mediaControllerCom : MediaControllerCompat? = null
    private var binder : RetranslatorService.ServiceBinder? = null

    private val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            binder = service as RetranslatorService.ServiceBinder
            srv = binder!!.service
            serviceBound = true
            try {
                mediaControllerCom = MediaControllerCompat(
                    this@MainActivity,
                    binder!!.getMediaSessionToken()
                )
            } catch (ex: RemoteException) {
                mediaController = null
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceBound = false
            binder = null
            if (mediaControllerCom != null) {
                mediaControllerCom = null
            }
        }

    }

    private fun loadPrefs(){
        val mSettings = getSharedPreferences(LoginActivity.APP_PREFERENCES, Context.MODE_PRIVATE)
        editor = mSettings.edit()
        ip = mSettings!!.getString(LoginActivity.APP_PREFERENCES_IP, "127.0.0.1")
        porttcp = mSettings.getString(LoginActivity.APP_PREFERENCES_PORT_TCP, resources.getString(R.string.defaultport))
            ?.let {
                Log.d("100", it)
                Integer.parseInt(it)
            }
        portudp = mSettings.getString(LoginActivity.APP_PREFERENCES_PORT_UDP, resources.getString(R.string.defaultport))
            ?.let {
                Log.d("100", it)
                Integer.parseInt(it)
            }
        md5Pass = mSettings.getString(LoginActivity.APP_PREFERENCES_PASSWORD_MD5, "")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadPrefs()
        setContentView(R.layout.activity_main)
        AndroidBug5497Workaround.assistActivity(this)
        connectStatus.text = String.format(resources.getString(R.string.connected), ip)

        if (playIntent == null) {
            playIntent = Intent(this, RetranslatorService::class.java)
            bindService(playIntent, serviceConnection, Context.BIND_AUTO_CREATE)
            startService(playIntent)
        }

        bindService(
            Intent(this, RetranslatorService::class.java),
            serviceConnection,
            Context.BIND_AUTO_CREATE
        )

        serviceSwitcher.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                serviceStatus.text =  resources.getString(R.string.service_on)
            } else {
                serviceStatus.text =  resources.getString(R.string.service_off)
            }
        }

        disconnect.setOnClickListener {
            this@MainActivity.runOnUiThread{
                editor!!.putBoolean(LoginActivity.APP_PREFERENCES_AUTH, false)
                editor!!.commit()
                val newIntent = Intent(this, LoginActivity::class.java)
                startActivity(newIntent)
                finish()
            }
        }

        playpause.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(
                            ip,
                            Integer.valueOf(porttcp!!),
                            Integer.valueOf(portudp!!)
                        )
                        val result = client.sendCommand(md5Pass, Command.PLAYPAUSE)
                    }
                }
            ).start()
        }

        prev.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(
                            ip,
                            Integer.valueOf(porttcp!!),
                            Integer.valueOf(portudp!!)
                        )
                        val result = client.sendCommand(md5Pass, Command.PREVIOUS)
                    }
                }
            ).start()
        }

        next.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(
                            ip,
                            Integer.valueOf(porttcp!!),
                            Integer.valueOf(portudp!!)
                        )
                        val result = client.sendCommand(md5Pass, Command.NEXT)
                    }
                }
            ).start()
        }

        vup.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(ip, porttcp!!, portudp!!)
                        val result = client.sendCommand(md5Pass, Command.VOLUME_UP)
                    }
                }
            ).start()
        }

        vdown.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(ip, porttcp!!, portudp!!)
                        val result = client.sendCommand(md5Pass, Command.VOLUME_DOWN)
                    }
                }
            ).start()
        }

        scr_off.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(ip, porttcp!!, portudp!!)
                        val result = client.sendCommand(md5Pass, Command.SCREEN_OFF)
                    }
                }
            ).start()
        }

        lock.setOnClickListener {
            Thread(
                Runnable{
                    run{
                        val client = SocketClient()
                        client.start(ip, porttcp!!, portudp!!)
                        val result = client.sendCommand(md5Pass, Command.LOCK)
                    }
                }
            ).start()
        }

        var cTimer: CountDownTimer? = null
        var timerStarted = false

        fun startTimer(shutdownB: Boolean) {
            timerStarted = true
            sleep.visibility = View.GONE
            cTimer = object : CountDownTimer(10000, 100) {

                override fun onTick(millisUntilFinished: Long) {
                    val f: Float = millisUntilFinished / 1000f
                    if (shutdownB) {
                        shutdown.setText(
                            String.format(
                                resources.getString(R.string.shutdow_after),
                                f
                            )
                        )
                    } else {
                        shutdown.setText(
                            String.format(
                                resources.getString(R.string.sleep_after),
                                f
                            )
                        )
                    }
                }

                override fun onFinish() {
                    shutdown.setText(resources.getString(R.string.shutdown))
                    sleep.visibility = View.VISIBLE
                    if (!shutdownB){
                        Thread(
                            Runnable{
                                run{
                                    val client = SocketClient()
                                    client.start(ip, porttcp!!, portudp!!)
                                    val result = client.sendCommand(md5Pass, Command.SLEEP)
                                }
                            }
                        ).start()
                    }
                }

            }.start()
            cTimer?.start()
        }

        fun cancelTimer() {
            timerStarted = false
            if (cTimer != null) cTimer?.cancel()
            shutdown.setText(resources.getString(R.string.shutdown))
            sleep.visibility = View.VISIBLE
        }

        shutdown.setOnClickListener {
            if (!timerStarted){
                startTimer(true)
                Thread(
                    Runnable{
                        run{
                            val client = SocketClient()
                            client.start(ip, porttcp!!, portudp!!)
                            val result = client.sendCommand(md5Pass, Command.SHUTDOWN)
                        }
                    }
                ).start()
            } else {
                cancelTimer()
                Thread(
                    Runnable{
                        run{
                            val client = SocketClient()
                            client.start(ip, porttcp!!, portudp!!)
                            val result = client.sendCommand(md5Pass, Command.SHUTDOWN_CANCEL)
                        }
                    }
                ).start()
            }
        }

        sleep.setOnClickListener {
            if (!timerStarted){
                startTimer(false)
            } else {
                cancelTimer()
            }
        }
    }

    override fun onDestroy() {
        stopService(playIntent)
        srv = null
        super.onDestroy()
    }
}
